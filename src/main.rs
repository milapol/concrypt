use gtk4::glib;
use gtk4::prelude::*;
use gtk4::{
    Align, Application, ApplicationWindow, Box, Button, Entry, Image, InputPurpose, Label,
    Orientation, Paned,
};
use image::Luma;
use magic_crypt::new_magic_crypt;
use magic_crypt::MagicCryptTrait;
use qrcode::QrCode;

struct DataEntry {
    layout: Box,
    entry: Entry,
}

fn main() {
    let app = Application::builder()
        .application_id("org.gtk.example")
        .build();
    app.connect_activate(setup_ui);

    app.run();
}

fn setup_ui(app: &Application) {
    let name: DataEntry = add_dataentry(&String::from("Name"));
    let phone: DataEntry = add_dataentry(&String::from("Phone"));
    let email: DataEntry = add_dataentry(&String::from("Email"));

    let (name_entry, phone_entry, email_entry) = (name.entry, phone.entry, email.entry);

    let pwd_label = Label::builder().label("Password").build();

    let pwd_entry = Entry::builder()
        .input_purpose(InputPurpose::Password)
        .visibility(false)
        .hexpand(true)
        .placeholder_text("Enter Password...")
        .build();

    let pwd_rtr_entry = Entry::builder()
        .input_purpose(InputPurpose::Password)
        .visibility(false)
        .hexpand(true)
        .placeholder_text("Reenter your Password...")
        .build();

    let img = Image::builder().vexpand(true).hexpand(true).build();

    let btn = Button::builder()
        .label("Generate")
        .margin_start(5)
        .margin_top(5)
        .margin_end(5)
        .margin_bottom(5)
        .vexpand(true)
        .valign(Align::End)
        .build();

    btn.connect_clicked(
        glib::clone!(@weak name_entry, @weak phone_entry, @weak email_entry, @weak img, @weak pwd_entry,@weak pwd_rtr_entry => move |_|{
            if pwd_entry.text_length() != 0  {
                if pwd_entry.text() == pwd_rtr_entry.text() {
                    let data = format!("{{ \n    name:\"{}\", \n    phone:\"{}\", \n    email:\"{}\" \n}}",name_entry.text(),phone_entry.text(),email_entry.text());
                    let mc = new_magic_crypt!(pwd_entry.text().as_str(),256);
                    let base64 = mc.encrypt_bytes_to_base64(data.as_bytes());
                    let save_path = "/tmp/qrcode.png";
                    let code = QrCode::new(base64.as_bytes()).unwrap();
                    let image = code.render::<Luma<u8>>().build();

                    image.save(save_path).unwrap();

                    img.set_file(Some(save_path));
                } else {
                        img.clear();
                        pwd_entry.set_text("");
                        pwd_rtr_entry.set_text("");
                }
            } else{
                let data = format!("{{ \n    name:\"{}\", \n    phone:\"{}\", \n    email:\"{}\" \n}}",name_entry.text(),phone_entry.text(),email_entry.text());
                let save_path = "/tmp/qrcode.png";
                let code = QrCode::new(data.as_bytes()).unwrap();
                let image = code.render::<Luma<u8>>().build();

                image.save(save_path).unwrap();

                img.set_file(Some(save_path));
            }
        }),
    );

    let pwd_box = Box::builder()
        .orientation(Orientation::Horizontal)
        .margin_start(5)
        .margin_top(5)
        .margin_end(5)
        .margin_bottom(5)
        .spacing(5)
        .hexpand(true)
        .build();

    let display_box = Box::builder()
        .orientation(Orientation::Vertical)
        .margin_start(5)
        .margin_top(5)
        .margin_end(5)
        .margin_bottom(5)
        .vexpand(true)
        .hexpand(true)
        .build();

    let data_box = Box::builder()
        .orientation(Orientation::Vertical)
        .margin_start(5)
        .margin_top(5)
        .margin_end(5)
        .margin_bottom(5)
        .build();

    let main_layout = Paned::builder()
        .start_child(&data_box)
        .end_child(&display_box)
        .position(275)
        .wide_handle(true)
        .build();

    let win = ApplicationWindow::builder()
        .application(app)
        .title("Concrypt")
        .default_width(920)
        .default_height(520)
        .build();

    data_box.append(&name.layout);
    data_box.append(&phone.layout);
    data_box.append(&email.layout);
    data_box.append(&btn);

    pwd_box.append(&pwd_label);
    pwd_box.append(&pwd_entry);
    pwd_box.append(&pwd_rtr_entry);

    display_box.append(&img);
    display_box.append(&pwd_box);

    win.set_child(Some(&main_layout));

    win.present();
}

fn add_dataentry(label: &String) -> DataEntry {
    let b = Box::builder()
        .orientation(Orientation::Horizontal)
        .spacing(5)
        .margin_top(2)
        .margin_bottom(2)
        .build();

    let l1 = Label::builder()
        .label(label)
        .width_request(70)
        .xalign(0.0)
        .build();

    let id = format!("entry_{}", label.replace(" ", "_").to_lowercase());
    let pt = format!("Enter {}...", label);

    let e1 = Entry::builder()
        .name(id.as_str())
        .placeholder_text(pt.as_str())
        .hexpand(true)
        .build();

    b.append(&l1);
    b.append(&e1);

    let dataentry = DataEntry {
        layout: b,
        entry: e1,
    };

    return dataentry;
}
